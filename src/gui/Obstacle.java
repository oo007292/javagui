package uk.ac.reading.oo007292.gui;
import javafx.scene.paint.Color;
/**
 * @author aisha12
 * This is a class class to create static objects for the balls to bounce off of 
 */
public class Obstacle extends Drone {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7821501632441898122L;

	/**
     * This draws the obstacle of those attributes in the specified colour
     * @param ix
     * @param iy
     * @param ir
     */
    public Obstacle(double ix, double iy, double ir) {
        super(ix, iy, ir);
        colour = "PURPLE";
    }
    
    /**
     * This will draw a rectangle onto the arena canvas
     * @param mc
     */
    public void drawDrone(ObjectCanvas mc) {
        mc.drawRect(x, y, radian, colour);
    }
    /** 
     * This gets the string of the object displayed as obstacle
     */
    
    protected String getStrType() {
        return "Obstacle";
    }

    /** 
     * This checks obstacle ball/drone but has no function
     */
    protected void checkDrone(Arena a) {
        // nothing to do
    }

    /** 
     * This adjusts obstacle ball/drone has no function
     */
    protected void adjustDrone() {
        // nothing to do
    }
}
