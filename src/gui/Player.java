package uk.ac.reading.oo007292.gui;

import javafx.scene.paint.Color;

/**
 * @author aisha12
 *This is a class to create the player/paddle object that will help to direct the particles and increase immunity
 */
public class Player extends Drone {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6357329314345464869L;
	/**This will set player drone/paddle size ir at ixpos,iypos position in the arena and determine colour of object
     * @param ix initial x position	
     * @param iy y pos
     * @param ir
     */
    public Player(double ixpos, double iypos, double ir) {
        super(ixpos, iypos, ir);
        colour = "BLUE";    
    }
 
    /**
     * This will draw a rectangle into the canvas at point x,y
     * @param mc
     */
    public void drawDrone(ObjectCanvas mc) {
        mc.drawRect(x, y, radian, colour);
    }

    /**
     * This is to check drone/ball but has nothing to do
     */
    @Override
    protected void checkDrone(Arena a) {
        // nothing to do
    }

    /**
     * This is to adjust drone/ball but has nothing to do
     */
    @Override
    protected void adjustDrone() {
        // nothing to do
    }
    /**
     * This will return string description as player
     */
    protected String getStrType() {
        return "Player";
    }
}
