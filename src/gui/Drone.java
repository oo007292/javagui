package uk.ac.reading.oo007292.gui;

import java.io.Serializable;

import javafx.scene.paint.Color;
/**
 * @author aisha12
 * This is a class to handle the size and ID of various drone types and pass down the collision functions
 */
public abstract class Drone implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8718366242967628399L;
	protected int droneID;							// sets the unique identifier for item
    protected double x, y, radian;						// sets the coordinates and size of the drones
    protected String colour;								// used to set colour
    static int counter = 0;						// used to give each ball a unique identifier

    Drone() {
        this(100, 100, 10);
    }
    /**
     * This will construct a drone/ball of radian ir at ix,iy of this colour
     * @param ix initial x pos
     * @param iy y pos
     * @param ir 
     */
    Drone(double ixpos, double iypos, double ir) {
        x = ixpos;
        y = iypos;
        radian = ir;
        droneID = counter++;			// identifier and increment class static
        colour = "GREEN";
    }
    /**
     * This will return x coordinate of the ball/drone
     * @return
     */
    public double getXd() { return x; }
    /**
     * This will return y coordinate of the ball/drone
     * @return
     */
    public double getYd() { return y; }
    /**
     * This will return radius of the ball/drone
     * @return
     */
    public double getRad() { return radian; }
    /**
     * This will set the ball at position nx,ny
     * @param nx
     * @param ny
     */
    public void setXY(double nx, double ny) {
        x = nx;
        y = ny;
    }
    /**
     * This will return the ID of the ball/drone
     * @return
     */
    public int getID() {return droneID; }
    /**
     * This will construct a ball/drone onto the arena
     * @param mc
     */
    public void drawDrone(ObjectCanvas mc) {
        mc.drawCircle(x, y, radian, colour);
    }
    protected String getStrType() {
        return "Ball";
    }
    /**
     * This will return string describing the drone/ball
     */
    public String toString() {
        return getStrType()+" at "+Math.round(x)+", "+Math.round(y);
    }
    /**
     * This is an abstract method for adjusting a ball/drone
     */
    protected abstract void adjustDrone();
    /**
     * abstract method for checking for a ball/drone in arena a
     * @param a
     */
    protected abstract void checkDrone(Arena a);
  
    /**
     * is an object at ox,oy size or colliding with this ball/drone
     * @param ox
     * @param oy
     * @param or
     * @return true if a hit takes place
     */
    public boolean hit(double ox, double oy, double or) {
        return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+radian)*(or+radian);
    }		// hit is made if dist between ball and ox,oy < dist rad + or

    /** has the ball/drone hit another ball/drone
     * @param oDrone - the other ball
     * @return true if a hit is made
     */
    public boolean hitting (Drone oDrone) {
        return hit(oDrone.getXd(), oDrone.getYd(), oDrone.getRad());
    }
}
