package uk.ac.reading.oo007292.gui;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author aisha12
 * This is a class to create the Arena to contain the objects to form the simulation 
 */
public class Arena implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8320968259579615806L;
	double xSizeA, ySizeA;						// these form the size of arena
    private ArrayList<Drone> allofD;			// this is the array list of all balls in arena
    /**
     * this will construct an arena of the specified size
     */
    Arena() {
        this(600, 500);			// default size is set 
    }
    /**
     * this will construct arena of size xS by yS and the position of the starting objects
     * @param xS
     * @param yS
     */
    Arena(double xS, double yS){
        xSizeA = xS;
        ySizeA = yS;
        allofD = new ArrayList<Drone>();					// this is the list of all drones/balls initially empty
        allofD.add(new Balls(xS/2, yS*0.8, 10, 35, 6));	// this will add small black balls
        allofD.add(new Enemy(xS/2, yS/2, 50));			// this will add enemy drone
        allofD.add(new Cure(xS/3, yS/10, 25));           // this will add cure drone
        allofD.add(new Player(xS/2, yS-20, 20));		// this will add player drone
        
        
        allofD.add(new Obstacle(xS/20, 1*yS/4.5, 18)); // this adds blocking objects/walls
        allofD.add(new Obstacle(xS/18, 1*yS/4.5, 18));
        allofD.add(new Obstacle(xS/1, 1*yS/4.5, 18));
        allofD.add(new Obstacle(xS/5, 1*yS/4.5, 18));
        allofD.add(new Obstacle(xS/3, 1*yS/4.5, 18));
        allofD.add(new Obstacle(xS, 1*yS/4.5, 18));
        allofD.add(new Obstacle(2*xS/3, 1*yS/4.5, 18));
        allofD.add(new Obstacle(2*xS/8, 1*yS/4.5,18));
        allofD.add(new Obstacle(xS-120, 1*yS/4.5, 18));
        allofD.add(new Obstacle(xS-60, 1*yS/4.5, 18));
        allofD.add(new Obstacle(xS-50, 1*yS/4.5, 18));
        allofD.add(new Obstacle(xS-20, 1*yS/4.5, 18));
        
     
        // this adds blocking objects/walls
        allofD.add(new Obstacle(xS/10, 1*yS/2.5, 16));
        allofD.add(new Obstacle(xS/4, 1*yS/2.5, 16));
        allofD.add(new Obstacle(2*xS/3, 1*yS/2.5, 16));
        allofD.add(new Obstacle(xS-80, 1*yS/2.5, 16));
       
        
    }
    
    /**
     * this will return arena size in the x direction
     * @return
     */
    public double getXd() {
        return xSizeA;
    }
    /**
     * this will return arena size in the y direction
     * @return
     */
    public double getYd() {
        return ySizeA;
    }
    /**
     * this will draw the arena on the canvas
     * @param mc
     */
    public void drawArena(ObjectCanvas mc) {
        for (Drone b : allofD) b.drawDrone(mc);		// this will draw all the balls/drones
    }
    /**
     * this will check all balls/drones, change angle of moving balls if needed
     */
    public void checkDrones() {
        for (Drone b : allofD) b.checkDrone(this);	// this will check all the balls/drones
    }
    /**
     * this will adjust for the different moving drones/balls
     */
    public void adjustDrones() {
        for (Drone b : allofD) b.adjustDrone();
    }
    /**
     * this will set the coordinates of the player drone
     * @param x
     * @param y
     */
    public void setPaddle(double x, double y) {
        for (Drone b : allofD)
            if (b instanceof Player) b.setXY(x, y);
    }
    /**
     * this will return list of strings defining each ball/drone
     * @return
     */
    public ArrayList<String> describeAll() {
        ArrayList<String> ans = new ArrayList<String>();		// this will set up empty arraylist
        for (Drone b : allofD) ans.add(b.toString());			// this will add string defining each ball
        return ans;												// this will return string list
    }
    /**
     * this checks the angle of the drone and its hit conditions depending on a wall or other drone/ball
     * @param x				ball x position
     * @param y				y
     * @param rad			radius
     * @param ang			current angle
     * @param notID			dont check id of the drone/ball
     * @return				new angle
     */
    public double CheckDroneAngle(double x, double y, double rad, double ang, int notID) {
        double ans = ang;
        if (x < rad || x > xSizeA - rad) ans = 180 - ans;
        // if ball hit (tried to go through) left or right walls, set mirror angle, being 180-angle
        if (y < rad || y > ySizeA - rad) ans = - ans;
        // if try to go off top or bottom, set mirror angle

        for (Drone b : allofD)
            if (b.getID() != notID && b.hit(x, y, rad)) ans = 180*Math.atan2(y-b.getYd(), x-b.getXd())/Math.PI;
        // check all balls except one with given id
        // if hitting, return angle between the other ball and this one.

        return ans;		// return the angle
    }

    /**
     * this will check if the enemy drone has been hit by another ball
     * @param the enemy drone
     * @return 	true if hit
     */
    public boolean checkHit(Drone target) {
        boolean ans = false;
        for (Drone b : allofD)
            if (b instanceof Balls && b.hitting(target)) ans = true;
        // try all balls
        return ans;
    }
    
    public boolean inBorder(Drone target) {
    	return !(target.getXd() - target.getRad() < 0 || target.getYd() - target.getRad() < 0 || target.getXd() + target.getRad() > xSizeA || target.getYd() + target.getRad() > ySizeA);
    }
    /**
     * this will add a new ball/drone at the specified coordinates
     * 
     */
    public void addDrone() {
        allofD.add(new Balls(xSizeA/2, ySizeA*0.8, 10, 50, 5));
    }
}
