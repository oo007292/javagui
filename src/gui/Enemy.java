package uk.ac.reading.oo007292.gui;
/**
 * @author aisha12
 * This class to create the moving enemy's success drone that shows the virus infecting the world and the amount of people dying
 */
public class Enemy extends Drone {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4065527064885866738L;
	private int death;
    private int directionofe = 1; // Determines left / right direction for target (-1 means left, 1 means right)
    private double speedofe = 4;
    /**
     * This determines direction and speed of the enemy's success drone
     */


    /**
     * This sets up the death count and colour of the created enemy's success drone
     * @param ixpos initial x position
     * @param iypos y pos
     * @param ir radius
     */
    public Enemy(double ixpos, double iypos, double ir) {
        super(ixpos, iypos, ir);
        death = 0;
        colour = "BLACK";
    }
    /**
     * This will return string defining the object as an enemy
     */
    protected String getStrType() {
        return "enemy";
    }

    /**
     * This will check ball/drone in the arena
     * @param a BallArena
     */
    @Override
    protected void checkDrone(Arena a) {
        if (a.checkHit(this)) {
        	death--;			// if been hit, then decrease amount of people alive
        }
        if (a.inBorder(this) == false) {
        	directionofe = -directionofe;
        }
    }
    /**
     * This will draw Ball and display death count
     */
    public void drawDrone(ObjectCanvas mc) {
        super.drawDrone(mc);
        mc.showInt(x, y, death);
    }

    /**
     * This will adjust the drone to a new direction
     */
    @Override
    protected void adjustDrone() {
        x += speedofe * directionofe;				
    }
  
}
