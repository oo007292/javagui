package uk.ac.reading.oo007292.gui;

/**
 * @author aisha12
 * This class to create the drone to show the amount of people who don't die from the virus, become immune and get cured
 */
public class Cure extends Drone {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6761213166187261352L;
	private int saved;
    private int directionofc = 1; // Determines left / right direction for cure drone to move at (-1 means left, 1 means right)
    private double speedofc = 3; // Determines the fastness of speed the cure drone moves at
    /**
     * This determines direction and speed of the cure drone
     */


    /**
     * This sets up the amount people saved and colour of the created cure drone
     * @param ixpos initial x pos
     * @param iypos y pos
     * @param ir radius
     */
    public Cure(double ixpos, double iypos, double ir) {
        super(ixpos, iypos, ir);
        saved = 0;
        colour = "WHITE";
    }
    /**
     * This will return string defining the object as a cure
     */
    protected String getStrType() {
        return "cure";
    }

    /**
     * This will check ball/drone in the arena
     * @param a BallArena
     */
    @Override
    protected void checkDrone(Arena a) {
        if (a.checkHit(this)) {
        	saved++;
        }// if been hit, then increase the amount of people saved
        if (a.inBorder(this) == false) {
        	directionofc = -directionofc;
        }
    }
    /**
     * This will draw Ball and display the amount of people saved
     */
    public void drawDrone(ObjectCanvas mc) {
        super.drawDrone(mc);
        mc.showInt(x, y, saved);
    }

    /**
     * This will adjust the drone to a new direction
     */
    @Override
    protected void adjustDrone() {
        x += speedofc * directionofc;				
    }
  
}
