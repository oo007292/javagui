package uk.ac.reading.oo007292.gui;
import javafx.scene.paint.Color;

/**
 * @author aisha12
 * This is a class to create drone/ball objects
 */
public class Balls extends Drone {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3930009900549004409L;
	double AngleofT, SpeedofT;			// angle and speed of travel of the balls/drones being created
    
    /** This will create balls, size ir,ixpos,iypos, moving at angle iangle and speed ispeed and determine colour given
     * @param ixpos initial x position
     * @param iypos y pos
     * @param ir radius
     * @param iangle initial angle of travel
     * @param ispeed speed
     */
    public Balls(double ixpos, double iypos, double ir, double iangle, double ispeed) {
        super(ixpos, iypos, ir);
        AngleofT = iangle;
        SpeedofT = ispeed;
        colour = "GREEN";
    }
    
    /**
     * This will draw a circle onto the canvas at point x,y
     * @param mc
     */
    public void drawDrone(ObjectCanvas mc) {
        mc.drawCircle(x, y, radian, colour);
    }

    /**
     * This will change angle of the ball/drone if it is hitting wall or another ball
     * @param a   ballArena
     */
    @Override
    protected void checkDrone(Arena a) {
        AngleofT = a.CheckDroneAngle(x, y, radian, AngleofT, droneID);
    }

    /**
     * This will adjust the ball/drone depending on its speed and angle
     */
    @Override
    protected void adjustDrone() {
        double radAngle = AngleofT*Math.PI/180;		// angle in radians
        x += SpeedofT * Math.cos(radAngle);		// new X,Y coordinates
        y += SpeedofT * Math.sin(radAngle);		
    }
    /**
     * This will return the string defining the drone
     */
    protected String getStrType() {
        return "Drone";
    }

}
